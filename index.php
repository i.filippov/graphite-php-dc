<?php

// composer autoload
include_once 'vendor/autoload.php';

// env example
$host = "graphite";
$port = "2003";

// code example
$config = new \Xcom\GraphiteRnd\GraphiteClientConfig([
    'host' => $host,
    'port' => $port,
]);

$graphiteClient = new \Xcom\GraphiteRnd\GraphiteClient($config);

$metricKey = 'xcom.test_metric';

mylog("Начинаем писать метрики");
while (true) {
    $randomMetricValue = rand(0, 100);

    $graphiteClient->sendValue($metricKey, $randomMetricValue);

    mylog("Записали в метрику {$metricKey} значение {$randomMetricValue}");

    sleep(1);
}

// functions

function mylog($string): void
{
    echo "\n" . "[" . date('d-m-Y H:i:s.u') . "]: " . $string;
}