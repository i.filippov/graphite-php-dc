# Graphite + PHP [RND]

Написан `index.php` + `docker-compose.yml`

## Комментарий к docker-compose.yml

Порт `9999` - web интерфейс Graphite, можно заменить на любой свободный TCP-порт. Нужен для работы Grafana.

## Разворачивание

Перед тем как начать убедитесь, что [Docker установлен](https://docs.docker.com/docker-for-mac/install/) и docker-compose на твоей системe.

Билдим имейджи
```shell
docker-compose build
```

Запускаем докер-композ
```shell
docker-compose up -d
```

Далее затягиваем php зависимости через `composer`
```shell
docker-compose run --rm php composer install
```

Потом не забудь через композер затянуть `vendor` папочку - `docker exec -it php composer install`

В директории `./configs-graphite/` будут созданы конфигурационные файлы.

Приложение работает с Graphite по протоколу UDP (2003), поэтому необходимо настроить демон сбора метрик `Carbon` для работы с UDP.

### Как настроить Carbon, чтобы работал с UDP:
- Останавливаем контейнер c графитом, например `docker stop graphite`
- В файле `./configs-graphite/carbon.conf` устанавливаем значение `ENABLE_UDP_LISTENER = True`
- Запускаем Graphite: `docker-compose up -d`

## Настройка grafana
Не е@у как, расскажешь, как раскуришь плз. Знаю только как метрики в гуе графита смотреть, но это зашквар.
Графит по идее легко интегрируется с графаной.

## Запуск пушера метрик в графит
```shell
docker-compose run --rm php php /var/www/html/index.php
```

## Просмотр метрик через UI графита
Как сказано выше, порт `9999`, то есть `http://localhost:9999`

Слева можно выбрать неймспейс метрик (после того, как они хоть раз будут отправлены)

Дальше не забудь поправить time range

![graphite ui](./docs/images/graphite-ui.png "Graphite UI")