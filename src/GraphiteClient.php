<?php

namespace Xcom\GraphiteRnd;

class GraphiteClient
{
    private const MESSAGE_FORMAT = "%s %s %s\n";

    /**
     * @var GraphiteClientConfig
     */
    protected GraphiteClientConfig $config;

    /**
     * @var array
     */
    protected array $pool = [];

    protected $socket;

    /**
     * @param GraphiteClientConfig $config
     */
    public function __construct(GraphiteClientConfig $config)
    {
        $this->config = $config;
        $this->socket = $this->getSocket();
    }


    /**
     * Sends data to the Graphite via sockets.
     *
     * @param string $key
     * @param string $value
     *
     * @return int
     */
    public function sendValue($key, $value)
    {
        $socket = $this->getSocket();
        if (!$socket) {
            return 0;
        }
        $message = sprintf(self::MESSAGE_FORMAT, $key, $value, time());

        return socket_send($socket, $message, mb_strlen($message), 0);
    }

    /**
     * @return resource
     */
    protected function getSocket()
    {
        if (!$this->socket) {
            $this->socket = $this->connect($this->config->host, $this->config->port);
        }

        return $this->socket;
    }

    /**
     * @param string $host
     * @param string $port
     *
     * @return null|resource
     */
    protected function connect(string $host, string $port)
    {
        $socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);

        socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, [
            'sec'  => 1,
            'usec' => 20,
        ]);

        if (!is_resource($socket)) {
            return null;
        }

        if (!socket_connect($socket, $host, $port)) {
            return null;
        }

        return $socket;
    }

    protected function disconnect()
    {
        if (is_resource($this->socket)) {
            socket_close($this->socket);
        }
    }

    public function __destruct()
    {
        $this->disconnect();
    }
}
