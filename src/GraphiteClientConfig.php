<?php

namespace Xcom\GraphiteRnd;

use Spatie\DataTransferObject\DataTransferObject;

class GraphiteClientConfig extends DataTransferObject
{
    /**
     * @var string
     */
    public string $host;

    /**
     * @var string
     */
    public string $port;
}